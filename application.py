import base64
import subprocess
import os
import signal
import logging
import json
import sys
from tinydb import Query, TinyDB
from flask import Flask, render_template, request, jsonify, flash, abort, \
    redirect, url_for
from flask_login import LoginManager, UserMixin, login_required, login_user, \
    logout_user, login_url
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired
from flask_bcrypt import Bcrypt
from urllib.parse import urlparse, urljoin

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False  # Does not crash with str and int keys
app.secret_key = base64.b64decode(os.environ.get('FLASK_SECRET',
                                                 base64.b64encode(b'\xd0R\xd6\x8fa}\x8c\xef\xc434]\xa9\t\xe9\xe5')))

logger = logging.getLogger('flask')
logger.setLevel(logging.INFO)
form = logging.Formatter('[%(asctime)s] [%(levelname)s] %(message)s')
hand = logging.StreamHandler(sys.stdout)
hand.setFormatter(form)
logger.addHandler(hand)

DISCORD_LOG = 'bot/discord.log'

PID_FILE = os.environ.get('PID_FILE', 'bot/bot.pid')

if os.path.isfile(PID_FILE):
    os.remove(PID_FILE)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

bcrypt = Bcrypt(app)

usr_db = TinyDB('users.json')


class User(UserMixin):
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def get_id(self):
        return self.username

    @classmethod
    def get(cls, name):
        DbUser = Query()
        usr = usr_db.get(DbUser.name == name)
        if not usr:
            return None
        return User(name, usr['password'])


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])


def make_hash(password):
    hashed = bcrypt.generate_password_hash(password)
    return hashed.decode('utf-8')


def check_hash(correct_hash, password):
    return bcrypt.check_password_hash(correct_hash.encode('utf-8'), password)


def is_safe_url(url):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, url))
    return test_url.scheme in ('http', 'https') and \
        ref_url.netloc == test_url.netloc


@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)


@login_manager.unauthorized_handler
def unauthorized():
    parsed = urlparse(request.url)
    if parsed.path.startswith('/api'):
        abort(403)
    else:
        flash(login_manager.login_message,
              category=login_manager.login_message_category)
        redirect_url = login_url(login_manager.login_view, next_url=request.url)
        return redirect(redirect_url)


@app.route("/")
@login_required
def bot_dashboard():
    return render_template('bot.html')


@app.route('/ping')
def ping():
    return 'pong!'


@app.route('/login', methods=['GET', 'POST'])
def login():
    login_form = LoginForm()
    if login_form.validate_on_submit():
        user = User.get(login_form.username.data)
        if user:
            if check_hash(user.password, login_form.password.data):
                login_user(user)
                next_path = request.args.get('next')
                if not is_safe_url(next_path):
                    abort(400)
                return redirect(next_path or url_for('bot_dashboard'))
    return render_template('login.html', form=login_form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/api/<name>', methods=['GET', 'POST'])
@login_required
def api(name):
    response = {}
    if name == 'status' and request.method == 'GET':
        if os.path.isfile(PID_FILE):
            with open(PID_FILE, 'r') as fil:
                pid = int(fil.readline())
                try:
                    os.kill(pid, 0)
                    response['status'] = 1
                except OSError:
                    logger.warning('PID file found, but process not running')
                    response['status'] = 0
        else:
            response['status'] = 0

    elif name == 'log' and request.method == 'GET':
        try:
            with open(DISCORD_LOG, 'r') as log:
                last_line = request.args.get('last_line')
                min_num = -1
                if last_line is not None:
                    try:
                        min_num = int(last_line)
                    except TypeError:
                        logger.error(f'Incorrect param in '
                                     f'/api/log: last_line={last_line}')
                    except ValueError:
                        logger.error(f'Incorrect param in '
                                     f'/api/log: last_line={last_line}')

                if min_num < 0:
                    min_num = -1

                if os.path.exists('bot/.rotated'):
                    response['rotated'] = True
                    min_num = 0
                    os.remove('bot/.rotated')

                response['running'] = os.path.exists(PID_FILE)
                line_num = 0
                for line in log:
                    line_dict = json.loads(line.rstrip().replace('\\n', '\\\\n'))
                    if line_num > min_num:
                        response[line_num] = line_dict
                    line_num += 1
        except OSError:
            response['error'] = True

    elif name == 'control' and request.method == 'POST':
        if request.form['action'] == 'start':
            if not os.path.isfile(PID_FILE):
                print(os.getcwd())
                proc = subprocess.Popen(['python', '-m', 'bot'], cwd='bot')
                with open(PID_FILE, 'w') as file:
                    file.write(str(proc.pid))
        elif request.form['action'] == 'stop':
            if os.path.isfile(PID_FILE):
                with open(PID_FILE, 'r') as file:
                    pid = int(file.readline())
                    try:
                        os.kill(pid, 0)
                        os.kill(pid, signal.SIGTERM)
                    except OSError:
                        logger.warning('Bot process not found, '
                                       'but PID file exists!')
    return jsonify(response)
