import os
import logging
import json_logging
import atexit
import sys
from discord import Message
from discord.ext import commands
from logging.handlers import RotatingFileHandler
from pathlib import Path
from tinydb import TinyDB, Query

from commands import Commands
from events import Events

json_logging.ENABLE_JSON_LOGGING = os.environ.get('JSON_LOGGING', False)
json_logging.init_non_web()

DISCORD_LOG = 'discord.log'

PID_FILE = os.environ.get('PID_FILE', 'bot.pid')


def cleanup():
    if os.path.isfile(PID_FILE):
        os.remove(PID_FILE)


def prefix(bot, message):
    guild_id = message.guild.id
    DbGuild = Query()
    guilds = bot.db.table('guilds')
    doc = guilds.get(DbGuild.id == guild_id)
    if doc and 'prefix' in doc:
        return doc['prefix']
    return bot.default_prefix


class DiscordBot(commands.Bot):
    def __init__(self):
        self.default_prefix = os.environ.get('COMMAND_PREFIX', '?')
        self._CONTROL_USER = os.environ.get('CONTROL_USER')

        if self._CONTROL_USER is None:
            import config
            self._CONTROL_USER = config.CONTROL_USER

        self._CONTROL_USER = int(self._CONTROL_USER)

        self._logger = logging.getLogger('discord')
        self._logger.setLevel(logging.INFO)
        self._logger.addHandler(logging.StreamHandler(sys.stdout))
        handler = RotatingFileHandler(DISCORD_LOG, backupCount=5)
        self._logger.addHandler(handler)
        handler.doRollover()
        Path('.rotated').touch()

        self.db = TinyDB('files/bot.json')

        atexit.register(cleanup)

        super(DiscordBot, self).__init__(prefix)

    async def on_ready(self):
        self._logger.info(f'Ready! I am \'{self.user}\' (ID={self.user.id}).')

    async def on_message(self, message: Message):
        if message.author == self.user or message.author.bot:
            return

        if 'kuumotus' in message.content.lower():
            self._logger.info(f'\'{message.author}\' (ID={message.author.id}) '
                              f'used Kuumotus!')
            await message.channel.send('nyt se kuumotus perseeseen')

        await self.process_commands(message)


client = DiscordBot()
secret = os.environ.get('BOT_SECRET')
if secret is None:
    import config
    secret = config.SECRET
client.add_cog(Commands(client))
client.add_cog(Events(client))
client.run(secret)
