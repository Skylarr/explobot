import logging
import typing
import aiohttp
from io import BytesIO
from urllib.parse import urlparse, ParseResult
from PIL import Image
from discord import Member, File, TextChannel, Attachment, Message
from discord.ext import commands
from tinydb import Query
from processors.tank import tankify, TooBigImage
from processors.meandtheboys import matbify
from concurrent.futures.process import ProcessPoolExecutor
from functools import partial

TIMEDELTA_MINUTE_LIMIT = 1440000000000  # Must be smaller than that

CHUNK_SIZE = 1024  # Chunk size when reading http request data


class Url(commands.Converter):
    async def convert(self, ctx, argument):
        try:
            parsed = urlparse(argument)
            if (parsed.scheme == 'http' or parsed.scheme == 'https') and \
                    parsed.netloc != '':
                return parsed
            else:
                raise commands.BadArgument(f'Incorrect url: \'{argument}\'')
        except ValueError as e:
            raise commands.BadArgument(f'Failed to parse url \'{argument}\'') \
                from e


class Commands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('discord')

    @commands.command(name='tank')
    async def tank(self, ctx, *,
                   tank_target: typing.Union[Member, Url] = None):
        """
        Tekee käyttäjästä tai mainitusta käyttäjästä tankin.
        """

        if tank_target is None:
            if len(ctx.message.attachments) > 0:
                tank_target = ctx.message.attachments[0]
            else:
                tank_target = ctx.author

        with BytesIO() as image_bytes, BytesIO() as final:
            recalculate_size = False
            if isinstance(tank_target, Member):
                self._logger.info(f'\'{ctx.author}\' (ID={ctx.author.id}) '
                                  f'requested tankification for '
                                  f'\'{tank_target}\' (ID={tank_target.id}) in '
                                  f'Guild \'{ctx.guild}\' (ID={ctx.guild.id}) '
                                  f'in channel \'{ctx.channel}\' '
                                  f'(ID={ctx.channel.id}).')
                avatar = tank_target.avatar_url_as(static_format='png')

                self._logger.debug(f'Downloading avatar picture.')
                # Save the avatar to memory
                await avatar.save(image_bytes)

            elif isinstance(tank_target, Attachment):
                if tank_target.height is None:
                    await ctx.send(f'{ctx.author.mention}, tiedoston pitää '
                                   f'olla kuva!')
                    return

                self._logger.info(f'\'{ctx.author}\' (ID={ctx.author.id}) '
                                  f'requested tankification for an attachment, '
                                  f'size {tank_target.size} bytes, filename '
                                  f'{tank_target.filename} in Guild '
                                  f'\'{ctx.guild}\' (ID={ctx.guild.id}) in '
                                  f'channel \'{ctx.channel}\' '
                                  f'(ID={ctx.channel.id}).')
                self._logger.debug('Downloading attachment picture.')
                await tank_target.save(image_bytes)
                recalculate_size = True

            elif isinstance(tank_target, ParseResult):
                # Async magic
                async with aiohttp.ClientSession() as session, \
                        session.get(tank_target.geturl()) as resp:
                    if 'Content-Length' not in resp.headers:
                        await ctx.send(f'{ctx.author.mention}, '
                                       f'linkkisi ei kelpaa!')
                        return

                    size = int(resp.headers['Content-Length'])

                    if size > 10 * (1024**2):  # Image must be max 10 MB
                        await ctx.send(f'{ctx.author.mention}, max 10 MB '
                                       f'linkkejä kiitos!')
                        return

                    self._logger.info(f'\'{ctx.author}\' (ID={ctx.author.id}) '
                                      f'requested tankification for an url '
                                      f'{tank_target.geturl()} in Guild '
                                      f'\'{ctx.guild}\' (ID={ctx.guild.id}) in '
                                      f'channel \'{ctx.channel}\' '
                                      f'(ID={ctx.channel.id}).')

                    self._logger.debug('Downloading linked picture.')

                    while True:
                        chunk = await resp.content.read(CHUNK_SIZE)
                        if not chunk:
                            break
                        image_bytes.write(chunk)

                    recalculate_size = True

            image_bytes.seek(0)

            target_image = Image.open(image_bytes)

            self._logger.debug('Manipulating images...')
            # Async magic
            with ProcessPoolExecutor(max_workers=1) as executor:
                tank = await self.bot.loop.run_in_executor(executor,
                            partial(tankify, target_image, recalculate_size))

            # Save the pic to memory and then send it
            tank.save(final, format='PNG', optimize=True)
            final.seek(0)

            self._logger.debug('Done! Sending the picture...')
            msg = await ctx.send(file=File(final, filename='tank.png'))
            self._logger.info(f'Done! '
                              f'Guild \'{msg.guild}\' (ID={msg.guild.id}), '
                              f'channel \'{msg.channel}\' '
                              f'(ID={msg.channel.id}), message ID={msg.id}')

    @commands.command(name='meandtheboys', aliases=['matb'])
    async def matb(self, ctx, members: commands.Greedy[Member], *, text=''):
        """
        Tekee Me and the boys -meemin annetuista käyttäjistä annetulla tekstillä.
        """
        avatars = []
        if len(members) not in [0, 1, 2, 3, 4]:
            await ctx.send(f'{ctx.author.mention}, mainitse 1-4 '
                           f'käyttäjää tai älä mainitse ketään!')
            return

        self._logger.info(f'\'{ctx.author}\' (ID={ctx.author.id}) requested me '
                          f'and the boys for {len(members)} users with text '
                          f'\'{text}\' in Guild \'{ctx.guild}\' '
                          f'(ID={ctx.guild.id}) in channel \'{ctx.channel}\' '
                          f'(ID={ctx.channel.id}).')

        with BytesIO() as avatar0_bytes, BytesIO() as avatar1_bytes, \
                BytesIO() as avatar2_bytes, BytesIO() as avatar3_bytes, \
                BytesIO() as final:
            all_bytes = [avatar0_bytes, avatar1_bytes,
                         avatar2_bytes, avatar3_bytes]
            if len(members) == 0:
                # Let's use the caller
                avatar = ctx.author.avatar_url_as(static_format='png')
                await avatar.save(avatar1_bytes)
                avatar1_bytes.seek(0)
                avatars.append(Image.open(avatar1_bytes).convert('RGBA'))
            else:
                for i in range(len(members)):
                    avatar = members[i].avatar_url_as(static_format='png')
                    await avatar.save(all_bytes[i])
                    all_bytes[i].seek(0)
                    avatars.append(Image.open(all_bytes[i]).convert('RGBA'))

            # Same async magic again
            with ProcessPoolExecutor(max_workers=1) as executor:
                final_image = await self.bot.loop.run_in_executor(executor,
                            partial(matbify, avatars, text))
            final_image.save(final, format='PNG', optimize=True)
            final.seek(0)
            msg = await ctx.send(file=File(final, filename='meandtheboys.png'))
            self._logger.info(f'Done! '
                              f'Guild \'{msg.guild}\' (ID={msg.guild.id}), '
                              f'channel \'{msg.channel}\' '
                              f'(ID={msg.channel.id}), message ID={msg.id}')

    @commands.command(name='gif-channel')
    @commands.has_permissions(administrator=True)
    async def gif_channel(self, ctx: commands.Context,
                          gif_channel: TextChannel = None):
        """
        Asettaa GIF-postauskanavan.
        """
        if gif_channel is None:
            gif_channel = ctx.channel
        DbGuild = Query()
        guilds = self.bot.db.table('guilds')
        guild_id = ctx.guild.id
        guilds.upsert(
            {
                'id': guild_id,
                'gif-channel': gif_channel.id
            },
            DbGuild.id == guild_id)

        self._logger.info(f'GIF channel for Guild '
                          f'\'{ctx.guild}\' (ID={ctx.guild.id}) is now '
                          f'\'{gif_channel}\' (ID={gif_channel.id}), '
                          f'according to '
                          f'\'{ctx.author}\' (ID={ctx.author.id}).')
        await ctx.send(f'GIF-kanava on nyt {gif_channel.mention}.')

    @commands.command(name='prefix')
    @commands.has_permissions(administrator=True)
    async def set_prefix(self, ctx, prefix=None):
        """
        Vaihtaa botin prefixiä.
        """
        if not prefix:
            await ctx.send('Anna prefix.')
            return

        DbGuild = Query()
        guilds = self.bot.db.table('guilds')
        guild_id = ctx.guild.id
        guilds.upsert(
            {
                'id': guild_id,
                'prefix': prefix
            },
            DbGuild.id == guild_id)
        self._logger.info(f'Prefix for Guild '
                          f'\'{ctx.guild}\' (ID={ctx.guild.id}) is now '
                          f'\'{prefix}\', according to '
                          f'\'{ctx.author}\' (ID={ctx.author.id}).')
        await ctx.send(f'Prefix on nyt {prefix}.')

    @commands.command(name='gif-time')
    @commands.has_permissions(administrator=True)
    async def gif_delay(self, ctx, time: int):  # FIXME: Only minutes allowed
        if time >= TIMEDELTA_MINUTE_LIMIT:
            await ctx.send(f'{ctx.author.mention}, luku voi olla maksimissaan '
                           f'{TIMEDELTA_MINUTE_LIMIT - 1}.')
            return

        DbGuild = Query()
        guilds = self.bot.db.table('guilds')
        guild_id = ctx.guild.id
        guilds.upsert(
            {
                'id': guild_id,
                'gif-delay': time
            },
            DbGuild.id == guild_id)
        self._logger.info(f'GIF delay for Guild '
                          f'\'{ctx.guild}\' (ID={ctx.guild.id}) is now '
                          f'{time} minutes, according to '
                          f'\'{ctx.author}\' (ID={ctx.author.id}).')
        await ctx.send(f'Maksimiaika liittymisen ja poistumisen välillä on nyt '
                       f'{time} minuuttia.')

    @commands.command(name='delete-log')
    @commands.has_permissions(administrator=True)
    async def set_delete_log(self, ctx, channel: TextChannel = None):
        if not channel:
            channel = ctx.channel

        DbGuild = Query()
        guilds = self.bot.db.table('guilds')
        guild_id = ctx.guild.id
        guilds.upsert(
            {
                'id': guild_id,
                'delete-channel': channel.id
            },
            DbGuild.id == guild_id)
        self._logger.info(f'Message delete channel for Guild '
                          f'\'{ctx.guild}\' (ID={ctx.guild.id}) is now '
                          f'\'{channel}\' (ID={channel.id}), '
                          f'according to '
                          f'\'{ctx.author}\' (ID={ctx.author.id}).')
        await ctx.send(f'Poistolokikanava on nyt {channel.mention}.')

    @tank.error
    async def tank_error(self, ctx, error):
        orig_error = getattr(error, 'original', error)

        if isinstance(orig_error, commands.BadUnionArgument):
            await ctx.send(f'{ctx.author.mention}, antamaasi käyttäjää ei '
                           f'löydy tai url ei kelpaa!')

        elif isinstance(orig_error, OSError):
            await ctx.send(f'{ctx.author.mention}, antamasi url:n takaa ei '
                           f'löydy kuvaa, tai jotain muuta kivaa tapahtui!')

        elif isinstance(orig_error, TooBigImage):
            await ctx.send(f'{ctx.author.mention}, antamasi kuva on liian iso!')

        else:
            self._logger.exception(f'Error in command tank.',
                                   exc_info=(type(orig_error), orig_error,
                                             orig_error.__traceback__))
