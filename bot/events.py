import logging
import discord
import datetime
from datetime import datetime, timedelta
from tinydb import Query
from discord import Member, Embed, Message, File
from discord.ext import commands
from io import BytesIO

# Maximum delay between join and leave
MAX_DELAY = timedelta(minutes=15)

# The embed the bot posts
simpson_embed = Embed()
simpson_embed.set_image(
    url='https://media.giphy.com/media/fDO2Nk0ImzvvW/giphy.gif')


class Events(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('discord')

    @commands.Cog.listener()
    async def on_member_remove(self, member: Member):
        now = datetime.utcnow()
        join = member.joined_at
        DbGuild = Query()
        guilds = self.bot.db.table('guilds')
        guild_id = member.guild.id
        doc = guilds.get(DbGuild.id == guild_id)

        if not doc or 'gif-delay' not in doc:
            delay = MAX_DELAY
        else:
            delay = timedelta(minutes=doc['gif-delay'])

        self._logger.debug('on_member_remove')
        self._logger.debug(f'UTC: {now}')
        self._logger.debug(f'Joined: {join}')
        self._logger.debug(f'Difference: {now - join}')
        if now - join <= delay:
            if not doc or 'gif-channel' not in doc:
                self._logger.debug(f'Guild {member.guild} '
                                   f'(ID={member.guild.id}) '
                                   f'does not have GIF channel set.')
                # TODO: Maybe send a message to a channel about this?
            else:
                channel_id = doc['gif-channel']
                channel = self.bot.get_channel(channel_id)
                self._logger.info(f'Sending GIF embed to Guild '
                                  f'\'{member.guild}\' (ID={member.guild.id}) '
                                  f'on channel \'{channel}\' '
                                  f'(ID={channel_id}).')
                await channel.send(embed=simpson_embed)

    @commands.Cog.listener()
    async def on_message_delete(self, message: Message):
        # Do not detect bot messages
        if message.author.bot:
            return

        DbGuild = Query()
        guilds = self.bot.db.table('guilds')
        guild_id = message.guild.id
        doc = guilds.get(DbGuild.id == guild_id)
        if not doc or 'delete-channel' not in doc:
            self._logger.debug(f'Guild {message.guild} (ID={message.guild.id}) '
                               f'does not have message delete channel set.')
            return

        embed = Embed(title='[POISTETTU VIESTI]', color=discord.Color.red())
        embed.add_field(name='Lähettäjä', value=message.author.mention)
        embed.add_field(name='Kanava', value=message.channel.mention)

        cont = message.content
        files = []
        if len(message.attachments) > 0:
            for attachment in message.attachments:
                with BytesIO() as data:
                    try:
                        await attachment.save(data, use_cached=True)
                        data.seek(0)
                        files.append(File(data, filename=attachment.filename))
                    except (discord.HTTPException, discord.NotFound):
                        self._logger.debug('Failed to save attachment')

            cont += f'\n{len(message.attachments)} liite' \
                    f'{"ttä" if len(message.attachments) > 1 else ""} ' \
                    f'({len(files)} tallennettu)'

        if len(message.embeds) > 0:
            cont += f'\n{len(message.embeds)} embed' \
                    f'{"iä" if len(message.embeds) > 1 else ""}'

        embed.add_field(name='Sisältö', value=cont, inline=False)

        embed.add_field(name='Luotu', value=message.created_at)
        if message.edited_at is not None:
            embed.add_field(name='Muokattu', value=message.edited_at)
        channel_id = doc['delete-channel']
        channel = self.bot.get_channel(channel_id)
        await channel.send(embed=embed, files=files)

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        """
        Tehty https://gist.github.com/EvieePy/7822af90858ef65012ea500bcecf1612:n
        pohjalta
        :param ctx: konteksti
        :param error: virhe
        :return: nah
        """
        # Jos jollain komennolla on oma handleri niin tätä ei tarvita
        if hasattr(ctx.command, 'on_error'):
            self._logger.debug('Command has their own error handler.')
            return

        # Allows us to check for original exceptions raised and sent to
        # CommandInvokeError. If nothing is found, we keep the exception
        # passed to on_command_error.
        orig_error = getattr(error, 'original', error)

        # Ignore
        if isinstance(orig_error, commands.CommandNotFound):
            return

        if isinstance(orig_error, commands.DisabledCommand):
            self._logger.info(f'\'{ctx.author}\' (ID={ctx.author.id}) tried to '
                              f'use command {ctx.command} in guild '
                              f'\'{ctx.guild}\' (ID={ctx.guild.id}) in channel '
                              f'\'{ctx.channel}\' (ID={ctx.channel.id}) but '
                              f'the command was disabled.')
            return await ctx.send(f'{ctx.author.mention}, {ctx.command}-'
                                  f'komento on poissa käytöstä.')

        elif isinstance(orig_error, commands.NoPrivateMessage):
            self._logger.info(f'\'{ctx.author}\' (ID={ctx.author.id}) tried to '
                              f'use command {ctx.command} in PM but it cannot '
                              f'be used there.')
            return await ctx.author.send(
                f'{ctx.command}-komentoa ei voi käyttää täällä.')

        elif isinstance(orig_error, commands.BadUnionArgument):
            self._logger.exception(f'Error in command {ctx.command}.',
                                   exc_info=(type(orig_error), orig_error,
                                             orig_error.__traceback__))
            if len(orig_error.errors) > 0:
                self._logger.error('This exception was caused by '
                                   'the following exceptions:')
                for index, error in enumerate(orig_error.errors):
                    self._logger.exception(f'Exception {index}:',
                                           exc_info=(type(error), error,
                                                     error.__traceback__))

            if ctx.command.qualified_name == 'tank':
                return await ctx.send(f'{ctx.author.mention}, antamaasi '
                                      f'käyttäjää ei löydy tai url ei kelpaa!')

        elif isinstance(orig_error, commands.BadArgument):
            if ctx.command.qualified_name == 'tank':
                return await ctx.send(f'{ctx.author.mention}, antamaasi '
                                      f'käyttäjää ei löydy.')
            elif ctx.command.qualified_name == 'gif-time':
                return await ctx.send(f'{ctx.author.mention}, anna numero!')

        elif isinstance(orig_error, commands.MissingPermissions):
            self._logger.info(f'\'{ctx.author}\' (ID={ctx.author.id}) tried to '
                              f'use command {ctx.command} in guild '
                              f'\'{ctx.guild}\' (ID={ctx.guild.id}) in channel '
                              f'\'{ctx.channel}\' (ID={ctx.channel.id}) but '
                              f'they didn\'t have enough permissions.')
            say = f'{ctx.author.mention}, sinulla ei ole tarpeeksi oikeuksia!' \
                  f' {ctx.command}-komento vaatii '
            if ctx.command.qualified_name in \
                    ['gif-channel', 'prefix', 'gif-time']:
                say += 'admin-oikeudet!'
            return await ctx.send(say)

        else:
            self._logger.exception(f'Error in command {ctx.command}.',
                                   exc_info=(type(orig_error), orig_error,
                                             orig_error.__traceback__))
