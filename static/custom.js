var log = {};
var last_line = -1;
var intervalId = null
var auto_scroll = true;

$(document).ready(function(e) {
    $.fn.appendText = function(text) {
        return this.each(function() {
            var textNode = document.createTextNode(text);
            $(this).append(textNode);
        });
    };
    $(".alert").alert();
    $("#start").click(function() {
        $.post("/api/control", {action: "start"}, function() {
            updateStatus();
            last_line = 0;
            log = {};
        });
    });
    
    $("#stop").click(function() {
        $.post("/api/control", {action: "stop"}, updateStatus)
    });
    
    updateStatus();
});

function updateStatus() {
    $.get("/api/status", function(resp) {
        let status = resp.status
        if (status === 1) {
            $("#status").text("ON");
            if (intervalId === null) {
                intervalId = setInterval(updateLog, 2000);
                updateLog();
            }
            $("#start").prop("disabled", true);
            $("#stop").prop("disabled", false);
        } else if (status === 0) {
            $("#status").text("OFF");
            if (intervalId !== null) {
                clearInterval(intervalId);
                intervalId = null;
            }
            $("#stop").prop("disabled", true);
            $("#start").prop("disabled", false);
        } else {
            $("#status").text("UNKNOWN");
        }
    });
}

function updateLog() {
    $.ajax({
        url: "/api/log",
        method: "GET",
        data: { last_line: last_line }
    }).done(function(data) {
        if (data.error !== undefined && data.error) {
            $("#log-area").empty();
            $("<h1/>", {
                "class": "row white-text justify-content-center",
                text: "Error loding the log",
                id: "error-text"
            }).appendTo("#log-area");
        } else {
            $("#error-text").remove();
        }
        if (data.rotated !== undefined && data.rotated) {
            $(".log-text").wrapAll($('<div/>', {
                "class": "rotated-text"
            }));
            $("#log-area hr").remove();
            $('<hr>', {
                "class": "rotate-divider"
            }).appendTo("#log-area");
        }
        for (line in data) {
            if (isInt(line)) {
                getMessageElement(
                    data[line].written_at, 
                    data[line].msg, 
                    data[line].level === "ERROR" ? 'message-red' : 'message')
                .appendTo("#log-area");

                if (data[line].exc_info !== undefined) {
                    let except_info = data[line].exc_info

                    elem = getMessageElement(data[line].written_at, "empty", "").children().last();
                    elem.empty();

                    for (exc_line of except_info.split("\\n")) {
                        let cls = 'message-red d-block'

                        if (exc_line.startsWith("    ")) {
                            cls += ' indent-2'
                        } else if (exc_line.startsWith("  ")) {
                            cls += ' indent-1'
                        }

                        elem.append($("<span/>", {
                            "class": cls,
                            text: exc_line.trimLeft()
                        }));
                    }
                    
                    elem.parent().appendTo("#log-area");
                }
                
                last_line = line;
                
            }
        }
        if (!data.running && intervalId !== null) {
            clearInterval(intervalId);
            intervalId = null;
            updateStatus();
        }
        if ($("#auto-scroll").prop("checked")) {
            $("#log-wrapper").scrollTop($("#log-wrapper")[0].scrollHeight);
        }
    }).fail(function(xhr, stat, err) {
        console.log(xhr);
        console.log(stat);
        console.log(err);
    });
}

function getMessageElement(time, msg, messageTextClass) {
    let div = $("<span/>", {
        "class": "white-text",
        text: "["
    })
    .wrap("<p/>")
    .parent()
    .append($("<span/>", {
        "class": "time",
        text: time
    }))
    .append($("<span/>", {
        "class": "white-text",
        text: "]"
    }))
    .wrap($("<div/>", {
        "class": "log-text d-flex"
    }))
    .parent();

    $("<span/>", {
        "class": messageTextClass + " d-block",
        text: msg
    })
    .wrap("<p/>")
    .parent()
    .appendTo(div);

    return div;
}

function isInt(value) { // Some magicc
    return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
}
